#!/usr/bin/env bash
cp -r vim ~/.vim
cp vimrc ~/.vimrc
cp tmux.conf ~/.tmux.conf
